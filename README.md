# Proyecto de ADOO

## Participacion de cada miembro

Bryan Dominguez y Tadeo Reyes desarrollaron el front end de la aplicacion, todo lo que se encuentra en la carpeta de views y la carpeta public/js, toda la logica para validar los datos de los usuarios en frontend, mandar las peticiones al backend y hacer el testeo de la aplicacion para que todo funcione correcto

Cuauhtemoc Sanchez desarrollo el backend de la aplicacion la conexion a la base de datos, la creacion de los models, los metodos de los controladores, el ruteo para las peticiooes entrantes y configuracion del servidor

Jared Alexis y Roberto Pacheco hicieron el analisis del sistema proporcionando al grupo de diseno los requerimientos del sistema, casos de uso asi como la elaboracion de todos los diagramas pertinentes, y la supervicion del desarrollo del sistema para garantizar que cumpla con los requerimientos establecidos

Aaron Flores y Jesus Dominguez con base a lo proporcionado por el grupo de analisis realizaron el diseno del sistema, tanto de las clases que se debian implementar, como los metodos, y los modelos para las tablas de la base de datos, asi como guiar y aclarar dudas al grupo de desarrollo

### Correr el proyecto

1.- Descargar el repositorio en su maquita

2.- Deben de tener instalado Node.js (Lo puedes descargar de aqui https://nodejs.org/en/, cualquiera de las dos versiones esta bien)

3.- Una vez descargado el proyecto en una terminal se situan en la carpeta del proyecto y dan el comando "npm install" (sin las comillas) el proyecto a a descargar unas dependencias

4.- Cuando termine de descargarlas dan el comando "npm start" (sin las comillas, antes hay que hacer los pasos de la configuracion de la base de datos)

5.- Para ver la pagina web en un navegador ingresar la url http://localhost:8000/

### Configurar la base de datos

Abrir el archivo "database.js" en cualquier editor de codigo, cambiar las credenciales por el mysql de su maquita (Igual y nada mas se ocuparia cambiar la contrasena, le ponen la que utilizan para entrar a mysql en POO2, la contrasena es donde dice "admin")
